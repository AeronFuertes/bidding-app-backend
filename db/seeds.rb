# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
3.times do 
  bidder = Bidder.create(name: Faker::Name.name, email: Faker::Internet.email, password: Faker::Internet.password)
  9.times do
    product = Product.create(name: Faker::Commerce.product_name, description: Faker::Lorem.sentences(number: 3), min_bid: Faker::Commerce.price(range: 10.0..20.0), start_bid: Faker::Commerce.price(range: 0..10.0), bid_expire: Faker::Time.forward(days: 5, format: :short))
  end
  3.times do
    bidder.auctions.create(your_bid: Faker::Commerce.price(range: 11.0..20.0), product_id: Faker::Number.number(digits: 1))
  end

end