class CreateAuctions < ActiveRecord::Migration[6.1]
  def change
    create_table :auctions do |t|
      t.integer :product_id
      t.integer :bidder_id
      t.integer :your_bid
      t.string :status

      t.timestamps
    end
  end
end
