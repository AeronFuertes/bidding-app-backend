class AddBidderIdToProducts < ActiveRecord::Migration[6.1]
  def change
    add_column :products, :bidder_id, :integer
  end
end
