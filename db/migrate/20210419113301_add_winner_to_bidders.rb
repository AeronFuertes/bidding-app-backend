class AddWinnerToBidders < ActiveRecord::Migration[6.1]
  def change
    add_column :bidders, :winner, :boolean, default: false
  end
end
