class CreateProducts < ActiveRecord::Migration[6.1]
  def change
    create_table :products do |t|
      t.string :name
      t.string :description
      t.integer :min_bid
      t.integer :start_bid
      t.date :bid_expire
      t.string :status

      t.timestamps
    end
  end
end
