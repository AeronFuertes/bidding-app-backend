class CreateBidders < ActiveRecord::Migration[6.1]
  def change
    create_table :bidders do |t|
      t.string :name
      t.string :email
      t.string :password_digest
      t.boolean :admin, default: false

      t.timestamps
    end
  end
end
