class ChangeDateBidExpireToDatetime < ActiveRecord::Migration[6.1]
  def change
    change_column :products, :bid_expire, :datetime
  end
end
