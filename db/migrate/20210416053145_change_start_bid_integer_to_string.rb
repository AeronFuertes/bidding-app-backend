class ChangeStartBidIntegerToString < ActiveRecord::Migration[6.1]
  def change
    change_column :products, :start_bid, :string
    change_column :auctions, :your_bid, :string
  end
end
