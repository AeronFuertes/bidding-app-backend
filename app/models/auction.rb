class Auction < ApplicationRecord
  belongs_to :bidder, validate: true
  belongs_to :product, validate: true

  validates :your_bid, presence: true
  
end
