class Product < ApplicationRecord
  belongs_to :bidder, optional: true
  has_many :auctions
  validates :name, presence: {strick: true, message: "Must give a product name"}
  validates :description, presence: {message: "Must give a description for this product"}

  validates :bid_expire, presence: true

  
end
