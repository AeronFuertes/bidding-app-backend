class Bidder < ApplicationRecord
  has_many :products
  has_many :auctions
  before_save { self.email = email.downcase }
  validates :name, presence: {strick: true, message: "Please enter your full name"},
               length: { minimum: 5, maximum: 50 }
  
  VALID_EMAIL_REGEX= /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true,
            length: { maximum:50, too_long: "%{count} characters is the maximum allowed"},
            uniqueness: { case_sensitive: false},
            format: { with: VALID_EMAIL_REGEX, message: "please enter a valid email"}

  validates :password, confirmation: {case_sensitive: true, message: "It didn't match"}
  
  has_secure_password
end
