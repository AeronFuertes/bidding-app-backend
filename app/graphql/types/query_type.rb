module Types
  class QueryType < Types::BaseObject
    # Add `node(id: ID!) and `nodes(ids: [ID!]!)`
    include GraphQL::Types::Relay::HasNodeField
    include GraphQL::Types::Relay::HasNodesField

    # Add root-level fields here.
    # They will be entry points for queries on your schema.

    # for bidders
    field :bidders, [Types::BidderType], null: false
    def bidders
      Bidder.all
    end

    field :bidder, Types::BidderType, null: false do
      argument :id, ID, required: true
    end

    def bidder(id:)
      Bidder.find(id)
    end

    #For Products
    field :products, [Types::ProductType], null: false
    def products
      Product.all
    end

    field :product, Types::ProductType, null: false do
      argument :id, ID, required: true
    end

    def product(id:)
      Product.find(id)
    end

    #For Auction
    field :auctions, [Types::AuctionType], null: false
    def auctions
      Auction.all
    end

    field :auction, Types::AuctionType, null: false do
      argument :id, ID, required: true
    end

    def auction(id:)
      Auction.find(id)
    end

    #For filters and pagination
    field :bidders, resolver: Resolvers::BidderSearch
    field :products, resolver: Resolvers::ProductSearch
    field :auctions, resolver: Resolvers::AuctionSearch
  end
end
