module Types
  class BidderType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: true
    field :email, String, null: true
    field :password_digest, String, null: true
    field :admin, Boolean, null: true
    field :winner, Boolean, null: true
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :products, [ProductType], null: false
    field :auctions, [AuctionType], null: false
  end
end
