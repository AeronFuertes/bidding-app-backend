module Types
  class MutationType < Types::BaseObject
    # TODO: remove me
    field :create_bidder, mutation: Mutations::CreateBidder
    field :update_bidder, mutation: Mutations::UpdateBidder
    
    field :create_product, mutation: Mutations::CreateProduct
    field :update_product, mutation: Mutations::UpdateProduct
    field :destroy_product, mutation: Mutations::DestroyProduct

    field :create_auction, mutation: Mutations::CreateAuction

    field :signin_user, mutation: Mutations::SignInUser
    
  end
end
