module Types
  class ProductType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: true
    field :description, String, null: true
    field :min_bid, Integer, null: true
    field :start_bid, String, null: true
    field :bid_expire, GraphQL::Types::ISO8601DateTime, null: true
    field :status, String, null: true
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :auction_by, BidderType, null: true, method: :bidder
    field :auctions, [Types::AuctionType], null: true
    field :bidder_id, Integer, null: true
  end
end
