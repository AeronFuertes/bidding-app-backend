module Types
  class AuctionType < Types::BaseObject
    field :id, ID, null: false
    field :product_id, Integer, null: true
    field :bidder_id, Integer, null: true
    field :your_bid, String, null: true
    field :status, String, null: true
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :auction_by, BidderType, null: true, method: :bidder
    field :product, ProductType, null: false
  end
end
