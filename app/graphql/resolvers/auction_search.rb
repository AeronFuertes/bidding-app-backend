require 'search_object'
require 'search_object/plugin/graphql'
class Resolvers::AuctionSearch < Resolvers::BaseSearchResolver
  # include SearchObject for GraphQL

  # scope is starting point for search
  scope { Auction.all }
  type types[Types::AuctionType]
  # inline input type definition for the advance filter
  class AuctionFilter < ::Types::BaseInputObject
    argument :OR, [self], required: false
    argument :your_bid_contains, String, required: false
    argument :product_id_contains, ID, required: false
  end
  # when "filter" is passed "apply_filter" would be called to narrow the scope
  option :filter, type: AuctionFilter, with: :apply_filter
  option :first, type: types.Int, with: :apply_first
  option :skip, type: types.Int, with: :apply_skip
 
  def apply_first(scope, value)
    scope.limit(value)
  end

  def apply_skip(scope, value)
    scope.offset(value)
  end
  # apply_filter recursively loops through "OR" branches
  def apply_filter(scope, value)
    branches = normalize_filters(value).reduce { |a, b| a.or(b) }
    scope.merge branches
  end
  def normalize_filters(value, branches = [])
    scope = Auction.all
    scope = scope.where('your_bid LIKE ?', "%#{value[:your_bid_contains]}%") if value[:your_bid_contains]
    scope = scope.where('product_id LIKE ?', "%#{value[:product_id_contains]}%") if value[:product_id_contains]

    branches << scope

    value[:OR].reduce(branches) { |s, v| normalize_filters(v, s) } if value[:OR].present?

    branches
   
  end
end