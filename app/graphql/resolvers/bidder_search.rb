require 'search_object'
require 'search_object/plugin/graphql'
class Resolvers::BidderSearch < Resolvers::BaseSearchResolver
  # include SearchObject for GraphQL

  # scope is starting point for search
  scope { Bidder.all }
  type types[Types::BidderType]
  # inline input type definition for the advance filter
  class BidderFilter < ::Types::BaseInputObject
    argument :OR, [self], required: false
    argument :name_contains, String, required: false
    argument :email_contains, String, required: false
    
  end
  # when "filter" is passed "apply_filter" would be called to narrow the scope
  option :filter, type: BidderFilter, with: :apply_filter
  option :first, type: types.Int, with: :apply_first
  option :skip, type: types.Int, with: :apply_skip
 
  def apply_first(scope, value)
    scope.limit(value)
  end

  def apply_skip(scope, value)
    scope.offset(value)
  end
  # apply_filter recursively loops through "OR" branches
  def apply_filter(scope, value)
    branches = normalize_filters(value).reduce { |a, b| a.or(b) }
    scope.merge branches
  end
  def normalize_filters(value, branches = [])
    scope = Bidder.all
    scope = scope.where('name LIKE ?', "%#{value[:name_contains]}%") if value[:name_contains]
    scope = scope.where('email LIKE ?', "%#{value[:email_contains]}%") if value[:email_contains]
    
    

    branches << scope

    value[:OR].reduce(branches) { |s, v| normalize_filters(v, s) } if value[:OR].present?

    branches
  end
end