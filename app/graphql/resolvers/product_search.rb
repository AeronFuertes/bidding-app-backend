require 'search_object'
require 'search_object/plugin/graphql'
class Resolvers::ProductSearch < Resolvers::BaseSearchResolver
  # include SearchObject for GraphQL

  # scope is starting point for search
  scope { Product.all }
  type types[Types::ProductType]
  # inline input type definition for the advance filter
  class ProductFilter < ::Types::BaseInputObject
    argument :OR, [self], required: false
    argument :name_contains, String, required: false
    argument :min_bid_contains, String, required: false
    argument :bid_expire_contains, String, required: false
  end
  # when "filter" is passed "apply_filter" would be called to narrow the scope
  option :filter, type: ProductFilter, with: :apply_filter
  option :first, type: types.Int, with: :apply_first
  option :skip, type: types.Int, with: :apply_skip
  # apply_filter recursively loops through "OR" branches
  def apply_first(scope, value)
    scope.limit(value)
  end

  def apply_skip(scope, value)
    scope.offset(value)
  end

  def apply_filter(scope, value)
    branches = normalize_filters(value).reduce { |a, b| a.or(b) }
    scope.merge branches
  end
  def normalize_filters(value, branches = [])
    scope = Movie.all
    scope = scope.where('name LIKE ?', "%#{value[:name_contains]}%") if value[:name_contains]
    scope = scope.where('description LIKE ?', "%#{value[:description_contains]}%") if value[:description_contains]
    scope = scope.where('min_bid LIKE ?', "%#{value[:min_bid_contains]}%") if value[:min_bid_contains]
    scope = scope.where('bid_expire LIKE ?', "%#{value[:bid_expire_contains]}%") if value[:bid_expire_contains]

    branches << scope

    value[:OR].reduce(branches) { |s, v| normalize_filters(v, s) } if value[:OR].present?

    branches
  end
end