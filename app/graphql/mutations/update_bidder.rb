class Mutations::UpdateBidder < Mutations::BaseMutation
  
    argument :name, String, required: true
    argument :admin, String, required: false
    argument :email, String, required: true
    argument :password, String, required: true
    argument :id, ID, required: false
  
  
    field :bidder, Types::BidderType, null: true
    
  
    def resolve(id: nil, name: nil, email: nil, password: nil, admin: false)
      bidder = Bidder.find(id)

      if bidder.update(name: name, email: email, password: password, admin: admin)
        { bidder: bidder}
      end
  
     
    end
end