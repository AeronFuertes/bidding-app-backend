module Mutations
    class CreateAuction < BaseMutation
        argument :your_bid, String, required: true
        argument :product_id, ID, required: false  
        argument :bidder_id, ID, required: false
        
        type Types::AuctionType

        def resolve(your_bid: nil, product_id: nil, bidder_id: nil)
            Auction.create!(your_bid: your_bid, product: Product.find(product_id), bidder_id: bidder_id)

        rescue ActiveRecord::RecordInvalid => e
            GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
        end
    end
end