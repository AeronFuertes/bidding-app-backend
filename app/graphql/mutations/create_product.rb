module Mutations
    class CreateProduct < BaseMutation
        argument :name, String, required: true
        argument :min_bid, Integer, required: false
        argument :start_bid, String, required: true
        argument :bid_expire, String, required: true
        argument :description, String, required: true
        argument :status, String, required: true

        
        type Types::ProductType

        def resolve(name: nil, min_bid: nil, start_bid: nil, bid_expire: nil, description: nil, status: nil)
            Product.create!(status: status, name: name, min_bid: min_bid, start_bid: start_bid, bid_expire: bid_expire, description: description)

        rescue ActiveRecord::RecordInvalid => e
            GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
        end

        
        

    end
end