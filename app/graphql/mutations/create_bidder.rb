class Mutations::CreateBidder < Mutations::BaseMutation
    class AuthProviderSignupData < Types::BaseInputObject
      argument :credentials, Types::AuthProviderCredentialsInput, required: false
    end
  
    argument :name, String, required: true
    argument :admin, String, required: false
    argument :winner, String, required: false
    argument :auth_provider, AuthProviderSignupData, required: false
    
  
    type Types::BidderType
    
  
    def resolve(name: nil, auth_provider: nil, admin: false, winner: false)
      Bidder.create!(name: name, email: auth_provider&.[](:credentials)&.[](:email), password: auth_provider&.[](:credentials)&.[](:password), admin: admin, winner: winner)
  
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
    end
end