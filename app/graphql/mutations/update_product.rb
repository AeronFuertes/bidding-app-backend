module Mutations
    class UpdateProduct < BaseMutation
        argument :id, ID, required: false
        argument :name, String, required: false
        argument :start_bid, String, required: false
        argument :bid_expire, String, required: false
        argument :description, String, required: false
        argument :status, String, required: false

        
        field :product, Types::ProductType, null: true


        def resolve(id: nil, name: nil, start_bid: nil, bid_expire: nil, description: nil, status: nil)
            product = Product.find(id)

            if product.update(name: name, start_bid: start_bid, bid_expire: bid_expire, description: description, status: status)
                { product: product }
            end
        end

        
        

    end
end