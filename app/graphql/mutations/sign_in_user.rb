module Mutations
    class SignInUser < BaseMutation
      null true
  
      argument :credentials, Types::AuthProviderCredentialsInput, required: false
  
      field :token, String, null: true
      field :bidder, Types::BidderType, null: true
  
      def resolve(credentials: nil)
        # basic validation
        return unless credentials
  
        user = Bidder.find_by email: credentials[:email]
  
        # ensures we have the correct user
        return unless user
        return unless user.authenticate(credentials[:password])
  
        token = AuthToken.token_for_user(user)
  
        
  
        context[:session][:token] = token
        { bidder: user, token: token }

      rescue ActiveRecord::RecordInvalid => e
        GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
      end
    end
  end