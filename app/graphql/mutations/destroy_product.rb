module Mutations
    class DestroyProduct < BaseMutation
        argument :id, ID, required: true
        
        
        type Types::ProductType

        def resolve(id: nil)
            Product.find(id).destroy

        rescue ActiveRecord::RecordInvalid => e
            GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
        end

        
        

    end
end